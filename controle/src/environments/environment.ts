// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAy2uPa_LT123Bdu4sPGHlWhZjWAFlcH44',
    authDomain: 'controle-daniel.firebaseapp.com',
    projectId: 'controle-daniel',
    storageBucket: 'controle-daniel.appspot.com',
    messagingSenderId: '388708057392',
    appId: '1:388708057392:web:31ac5955f7f7fe41aa88f5'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
